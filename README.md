# Unity Obstacle Course Game Template

### Demo: http://otherwise-studios.com/demos/unity/obstacle-course-template/1/

![Example Image](http://otherwise-studios.com/img/obstacle-course-game.png)

This is a repository with a series of scripts and scenes in Unity that help
you develop an Obstacle Course Game (Like Subway Surfers), without having
to start from scratch.

## Scripts

### Character

* **Character.cs**: 
This script handles the life.

* **Character_Input.cs**:
This is the script incharged of handling the keyboard/controllers/touch input
and make the character move, jump or slide.

* **Character_Motor.cs**:
This handles the jumping, moving, sliding behaviours. It supports gestures and keyboard input.
It also introduces what I call "Track System". Here is a demonstration of how it works. In the game, you will have for example 3 tracks for the player tu run on, like so:
	  player
	  1      2     3
	  |      |     |

if you move to the right you will have:

	       player
	  1      2     3
	  |      |     |

if you move back to the left you will have:

	  player
	  1      2     3
	  |      |     |
	 
### Camera

* **Camera_LookAtPlayer.cs**: 
Looks at the player.


## Motivation

I make this project because I like programming videogames and I think it would be useful for me in the future
once I graduate from the university, but if you find it useful use it too.


## License

The MIT License (MIT)

Copyright (c) 2014 Alf Eaton

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## Credits

Made By Juan Camilo Guarín Peñaranda
@jcguarinp, fb.com/jcguarinpenaranda
Cali, Colombia
