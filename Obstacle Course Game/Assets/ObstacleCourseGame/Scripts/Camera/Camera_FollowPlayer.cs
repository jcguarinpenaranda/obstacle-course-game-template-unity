﻿using UnityEngine;
using System.Collections;

public class Camera_FollowPlayer : MonoBehaviour {


	public Transform player;

	//The distance of the camera to the player
	public float distance=5;

	//The height between the player position
	//and the camera.
	public float height=3;


	//Follow behind or follow facing the player
	public bool followBehind = true;


	// Update is called once per frame
	void Update () {

		transform.LookAt (player.position);

		Vector3 pos;
		

		if (followBehind) {
			pos = player.position - player.forward * -1 * distance;
		} else {
			pos = player.position - player.forward * distance;
		}


		pos = pos+Vector3.up*height;

		transform.position = pos;

	}

}
