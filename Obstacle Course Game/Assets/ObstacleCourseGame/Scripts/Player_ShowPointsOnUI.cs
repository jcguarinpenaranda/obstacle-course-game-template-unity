﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Player))]
public class Player_ShowPointsOnUI : MonoBehaviour {

	public Text uiText;

	Player player;

	// Use this for initialization
	void Start () {

		player = GetComponent<Player> ();

	}

	void PickedCoin(){
		if (uiText) {
			uiText.text = ""+ player.points;
		}

		print ("picked");

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
