﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Player))]
public class Player_PickCoin : MonoBehaviour {

	Player player;
	// Use this for initialization
	void Start () {
		player = GetComponent<Player> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider c){
		if (c.tag == "Coin") {
			player.points += c.GetComponent<Coin>().value;
			BroadcastMessage("PickedCoin");
		}
	}
}
