﻿using UnityEngine;
using System.Collections;

public class Character_Motor : MonoBehaviour {

	public float jumpImpulse=5;
	public int numJumps = 2;
	public bool allowMovementOnJumping = true;

	private bool jumping;
	private int numJumpsNow = 0;

	public float characterHeight= 2.0f;

	private Rigidbody rigidBody;

	public float moveRightLeftMultiplier = 20.0f;

	public bool run = true;
	public float runSpeed = 20.0f;




	/*
	 * The track system that makes 
	 * the player to move to one track
	 * to another. 
	 * 
	 * In this case we make a 3 track system.
	 */
	public int currentTrack = 1; // 1,2,3 (left, center, right)
	public float trackSize = 2; //in meters
	public int numTracks=3; //The 3 tracks



	// Use this for initialization
	void Start () {
		rigidBody = GetComponent<Rigidbody> ();

		/*
		 * Here, we start the coroutine that checks 
		 * if the player is on the floor. 
		 * Note this is not being called every frame
		 * in the update to reduce calculations
		 */
		StartCoroutine ("CheckIfOnFloorCoroutine");

	}
	

	void Update () {

		if (run && !jumping) {
			if(rigidBody.velocity.magnitude<runSpeed){
				rigidBody.AddForce(gameObject.transform.forward * runSpeed, ForceMode.Acceleration);
			}
		}
		
	}


	/*
	 * This function makes the character jump.
	 */
	public void Jump(){
		if (numJumpsNow < numJumps) {

			if(!jumping){
				jumping = true;
			}

			numJumpsNow ++;
			rigidBody.AddForce (Vector3.up * jumpImpulse, ForceMode.Impulse);
		}
	}


	/*
	 * This function checks if the player is on the floor
	 * based on rays shot downwards
	 */
	private void CheckIfOnFloor(){
		Ray ray = new Ray (this.transform.position, Vector3.down);
		RaycastHit r = new RaycastHit ();
		
		if (Physics.Raycast (ray, out r)) {
			//print ("distancia: "+r.distance+", escala y:" + characterHeight);
			if(r.distance<=characterHeight/2+0.05f){
				numJumpsNow = 0;
				jumping = false;
			}
		}
	}

	private IEnumerator CheckIfOnFloorCoroutine() {
		for(;;) {
			CheckIfOnFloor();
			yield return new WaitForSeconds(.1f);
		}
	}
	
	
	/*
	 * This method displaces the player
	 * to the right track
	 */
	public void MoveToRightTrack(){
		if (currentTrack <numTracks && (!jumping || allowMovementOnJumping)) {

				currentTrack ++;

				Debug.Log(currentTrack);

				StartCoroutine("MoveRightCoroutine");
		}
	}

	/*
	 * This method displaces the player to
	 * the left track
	 */
	public void MoveToLeftTrack(){
		if (currentTrack > 1 && (!jumping || allowMovementOnJumping)) {
			currentTrack--;

			Debug.Log(currentTrack);
			
			StartCoroutine("MoveLeftCoroutine");

		}
	}

	private IEnumerator MoveRightCoroutine(){
		for (float f = 1f; f >= 0; f -= 0.1f) {
			rigidBody.transform.position+=(gameObject.transform.right*trackSize*0.1f);
			yield return null;
		}
	}

	private IEnumerator MoveLeftCoroutine(){
		for (float f = 1f; f >= 0; f -= 0.1f) {
			rigidBody.transform.position-=(gameObject.transform.right*trackSize*0.1f);
			yield return null;
		}
	}


	/*
	 * The following functions are supposed
	 * to be called from a situation where
	 * the track system is not being used
	 */

	public void MoveRight(float sensitivity){
		if (jumping || allowMovementOnJumping) {
			rigidBody.AddForce(gameObject.transform.right*sensitivity*moveRightLeftMultiplier, ForceMode.Acceleration);	
		}
	}

	public void MoveLeft(float sensitivity){
		if (!jumping || allowMovementOnJumping) {
			rigidBody.AddForce(gameObject.transform.right*-1*sensitivity*moveRightLeftMultiplier, ForceMode.Acceleration);	
		}
	}

	public void Move(float dir){
		if (!jumping || allowMovementOnJumping) {
			rigidBody.AddForce(gameObject.transform.right*dir*moveRightLeftMultiplier, ForceMode.Acceleration);	
		}
	}

}
