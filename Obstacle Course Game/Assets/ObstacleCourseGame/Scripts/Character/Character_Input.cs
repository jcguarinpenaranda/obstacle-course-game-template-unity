﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Character))]
[RequireComponent(typeof(Character_Motor))]
public class Character_Input : MonoBehaviour {

	/*
	 * Will the player move also with
	 * the keyboard? true or false
	 */
	public bool useKeyBoard = true;

	/*
	 * If useTrackSystem is enabled, this will
	 * enable the right and left swipe gestures.
	 * 
	 * No matter if useTrackSystem is enabled or 
	 * not, this will make the player jump or crouch
	 * when swipe up or down respectively.
	 */
	public bool useSwipe = true;

	/*
	 * This only works if useTrackSystem is disabled.
	 */
	public bool useAccelerometer = false;

	/*
	 * The sensitivity of the accelerometer
	 * Bigger values make the player go faster
	 * to the right and to the left
	 */
	public float accelerometerMultiplier=2;



	/*
	 * This activates the track system. Basically,
	 * it is what tells the player to run on a given
	 * set of tracks. If this is false, then the
	 * player will move little by little to the left
	 * or to the right, but if
	 * it is true then the player will move according
	 * to the track system.
	 * 
	 * To learn more about the track system take a look
	 * at Character_Motor.cs. 
	 * 
	 * Let's imagine the track system is this:
	 * 
	 * player
	 * 1      2     3
	 * |      |     |
	 * 
	 * if you move to the right you will have:
	 * 
	 *      player
	 * 1      2     3
	 * |      |     |
	 *
	 * if you move back to the left you will have:
	 * 
	 * player
	 * 1      2     3
	 * |      |     |
	 */
	public bool useTrackSystem = true;

	

	private Character_Motor motor;


	void Start () {
		motor = GetComponent<Character_Motor> ();
	}
	

	void Update () {

		if (useAccelerometer && !useTrackSystem) {
			motor.Move(Input.acceleration.x * accelerometerMultiplier);
		}

		if (useKeyBoard) {
			if(Input.GetButtonDown("Jump")){
				motor.Jump();
			}

			if(useTrackSystem){
				if(Input.GetKeyDown(KeyCode.RightArrow)){
					motor.MoveToRightTrack();
				}else if(Input.GetKeyDown(KeyCode.LeftArrow)){
					motor.MoveToLeftTrack();
				}

			}else{
				motor.Move(Input.GetAxis("Horizontal"));		
			}

		}




	}

	public void OnFingerSwipe(Lean.LeanFinger finger)
	{

			// Store the swipe delta in a temp variable
			var swipe = finger.SwipeDelta;
			
			if (swipe.x < -Mathf.Abs(swipe.y) && useTrackSystem && useSwipe)
			{
				//InfoText.text = "You swiped left!";
				motor.MoveToLeftTrack();
			}
			
			if (swipe.x > Mathf.Abs(swipe.y) && useTrackSystem &&useSwipe)
			{
				//InfoText.text = "You swiped right!";
				motor.MoveToRightTrack();
			}
			
			if (swipe.y < -Mathf.Abs(swipe.x) && useSwipe)
			{
				//InfoText.text = "You swiped down!";
			}
			
			if (swipe.y > Mathf.Abs(swipe.x) && useSwipe)
			{
				//InfoText.text = "You swiped up!";
				motor.Jump();
			}

	}


	protected virtual void OnEnable()
	{
		// Hook into the OnSwipe event
		Lean.LeanTouch.OnFingerSwipe += OnFingerSwipe;
	}
	
	protected virtual void OnDisable()
	{
		// Unhook into the OnSwipe event
		Lean.LeanTouch.OnFingerSwipe -= OnFingerSwipe;
	}

}
