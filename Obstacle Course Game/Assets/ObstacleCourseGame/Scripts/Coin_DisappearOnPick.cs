﻿using UnityEngine;
using System.Collections;

public class Coin_DisappearOnPick : MonoBehaviour {

	public string playerTag= "Player";

	void OnTriggerEnter(Collider c){
		if(c.tag == playerTag){
			Destroy(gameObject);
		}
	}
}
